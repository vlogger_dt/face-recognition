import face_recognition
from PIL import Image,ImageDraw

imgBill = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/bill-gates.jpg')
imgBill_encoding = face_recognition.face_encodings(imgBill)[0]

imgElon = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/elon-musk.jpg')
imgElon_encoding = face_recognition.face_encodings(imgElon)[0]

imgSteve = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/steve-jobs.jpg')
imgSteve_encoding = face_recognition.face_encodings(imgSteve)[0]

known_face_encodings = [imgBill_encoding,imgSteve_encoding,imgElon_encoding]
known_face_names = ['Bill Gates','Steve Jobs','Elon Musk']

testImg = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/steve-bill-elon-mark.jpg')

face_locations = face_recognition.face_locations(testImg)
face_encodings = face_recognition.face_encodings(testImg,face_locations)


# Convert to PIL Format
pil_image = Image.fromarray(testImg)

# Create a ImageDraw Instance
draw = ImageDraw.Draw(pil_image)

# Loop through faces in test image

for (top,right,bottom,left),face_encoding in zip(face_locations,face_encodings):
	matches = face_recognition.compare_faces(known_face_encodings,face_encoding)
	name = 'Unknown'
	if True in matches:
		first_match_index = matches.index(True)
	name = known_face_names[first_match_index]

	# Draw Box
	draw.rectangle(((left,top),(right,bottom)),outline=(255,0,0))

	# Draw Label
	text_width ,text_height = draw.textsize(name)
	draw.rectangle(((left,bottom- text_height -10),(right,bottom)),fill=(0,255,0),outline=(0,0,255))
	draw.text((left+6,bottom- text_height-5),name,fill=(0,0,0))

del draw

pil_image.show()

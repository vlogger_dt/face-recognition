import cv2
import numpy as np
import face_recognition
from PIL import Image,ImageDraw

font = cv2.FONT_HERSHEY_SIMPLEX

imgDurgesh = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/IMG_0284.jpg')
imgDurgesh_encoding = face_recognition.face_encodings(imgDurgesh)[0]

imgAlban = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/alban.jpg')
imgAlban_encoding = face_recognition.face_encodings(imgAlban)[0]

imgPrashant = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/Prashant.jpg')
imgPrashant_encoding = face_recognition.face_encodings(imgPrashant)[0]

imgAbhishek = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/Abhishek.jpg')
imgAbhishek_encoding = face_recognition.face_encodings(imgAbhishek)[0]

imgAnkita = face_recognition.load_image_file('/Users/durgeshthakur/Downloads/Ankita.jpg')
imgAnkita_encoding = face_recognition.face_encodings(imgAnkita)[0]

known_face_encodings = [imgDurgesh_encoding,imgAlban_encoding,imgPrashant_encoding,imgAbhishek_encoding,imgAnkita_encoding]
known_face_names = ['Durgesh','Alban','Prashant','Abhishek','Ankita']

emp_attendace = {}

cam = cv2.VideoCapture(0)

while True:
    ret, img =cam.read()
    small_frame = cv2.resize(img,(0,0),fx=0.25,fy=0.25)
    rgb_small_frame = small_frame[:,:,::-1]
    face_locations = face_recognition.face_locations(rgb_small_frame)
    face_encodings = face_recognition.face_encodings(rgb_small_frame,face_locations)
    
    for (top,right,bottom,left),face_encoding in zip(face_locations,face_encodings):
        top*=4
        bottom*=4
        left*=4
        right*=4
        
        matches = face_recognition.compare_faces(known_face_encodings,face_encoding)
        name = 'Unknown'
        if True in matches:
            first_match_index = matches.index(True)
            name = known_face_names[first_match_index]
        else:
            name='Unknown'
        cv2.rectangle(img,(left,top),(right,bottom),(255,0,0),5)
        cv2.putText(img, name, (left+2,top+23), font, 1, (255,255,255), 3)
        cv2.imshow('camera',img)

    

    k = cv2.waitKey(10) & 0xff # Press 'ESC' for exiting video
    if k == 27:
        break

cam.release()
cv2.destroyAllWindows()


